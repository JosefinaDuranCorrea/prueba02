/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Josefina
 */
@Entity
@Table(name = "ingresopersona")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Ingresopersona.findAll", query = "SELECT i FROM Ingresopersona i"),
    @NamedQuery(name = "Ingresopersona.findByNombre", query = "SELECT i FROM Ingresopersona i WHERE i.nombre = :nombre"),
    @NamedQuery(name = "Ingresopersona.findByApellido", query = "SELECT i FROM Ingresopersona i WHERE i.apellido = :apellido"),
    @NamedQuery(name = "Ingresopersona.findByEdad", query = "SELECT i FROM Ingresopersona i WHERE i.edad = :edad"),
    @NamedQuery(name = "Ingresopersona.findByCarrera", query = "SELECT i FROM Ingresopersona i WHERE i.carrera = :carrera"),
    @NamedQuery(name = "Ingresopersona.findByCorreo", query = "SELECT i FROM Ingresopersona i WHERE i.correo = :correo")})
public class Ingresopersona implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "nombre")
    private String nombre;
    @Size(max = 2147483647)
    @Column(name = "apellido")
    private String apellido;
    @Size(max = 2147483647)
    @Column(name = "edad")
    private String edad;
    @Size(max = 2147483647)
    @Column(name = "carrera")
    private String carrera;
    @Size(max = 2147483647)
    @Column(name = "correo")
    private String correo;

    public Ingresopersona() {
    }

    public Ingresopersona(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getEdad() {
        return edad;
    }

    public void setEdad(String edad) {
        this.edad = edad;
    }

    public String getCarrera() {
        return carrera;
    }

    public void setCarrera(String carrera) {
        this.carrera = carrera;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (nombre != null ? nombre.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Ingresopersona)) {
            return false;
        }
        Ingresopersona other = (Ingresopersona) object;
        if ((this.nombre == null && other.nombre != null) || (this.nombre != null && !this.nombre.equals(other.nombre))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cl.entity.Ingresopersona[ nombre=" + nombre + " ]";
    }
    
}
