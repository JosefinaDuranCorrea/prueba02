/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.Dao;

import cl.Dao.exceptions.NonexistentEntityException;
import cl.Dao.exceptions.PreexistingEntityException;
import cl.entity.Ingresopersona;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author Josefina
 */
public class IngresopersonaJpaController implements Serializable {

    public IngresopersonaJpaController() {
       // this.emf = emf;
    }
    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("my_persistence_unit");

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Ingresopersona ingresopersona) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(ingresopersona);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findIngresopersona(ingresopersona.getNombre()) != null) {
                throw new PreexistingEntityException("Ingresopersona " + ingresopersona + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Ingresopersona ingresopersona) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            ingresopersona = em.merge(ingresopersona);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = ingresopersona.getNombre();
                if (findIngresopersona(id) == null) {
                    throw new NonexistentEntityException("The ingresopersona with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Ingresopersona ingresopersona;
            try {
                ingresopersona = em.getReference(Ingresopersona.class, id);
                ingresopersona.getNombre();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The ingresopersona with id " + id + " no longer exists.", enfe);
            }
            em.remove(ingresopersona);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Ingresopersona> findIngresopersonaEntities() {
        return findIngresopersonaEntities(true, -1, -1);
    }

    public List<Ingresopersona> findIngresopersonaEntities(int maxResults, int firstResult) {
        return findIngresopersonaEntities(false, maxResults, firstResult);
    }

    private List<Ingresopersona> findIngresopersonaEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Ingresopersona.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Ingresopersona findIngresopersona(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Ingresopersona.class, id);
        } finally {
            em.close();
        }
    }

    public int getIngresopersonaCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Ingresopersona> rt = cq.from(Ingresopersona.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
