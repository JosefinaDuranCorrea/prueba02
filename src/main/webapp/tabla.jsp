<%-- 
    Document   : tabla
    Created on : 5/06/2021, 05:09:41 PM
    Author     : Josefina
--%>

<%@page import="java.util.Iterator"%>
<%@page import="cl.entity.Ingresopersona"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%

    List<Ingresopersona> lista = (List<Ingresopersona>) request.getAttribute("persona");
    Iterator<Ingresopersona> itIngresopersona = lista.iterator();

%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
        <title>JSP Page</title>
    </head>
    <body>
        <form name="form" action="tablaController" method="GET">   
            <table align="center" width="1000">
                <tr>
                    <td>
                        <h2>Listado de Estudiantes</h2>
                        <fieldset>
                            <table class="default" border="2" align="center" width="800" bgcolor="#e5e5e5">
                                <tr bgcolor="#FFF">
                                    <td colspan="6" align="right"><a href="index.jsp">Ingresar nuevo</a></td>
                                </tr>
                                <tr>
                                    <th>Nombre</th>
                                    <th>Apellido</th>
                                    <th>Edad</th>
                                    <th>Carrera</th>
                                    <th>Correo electronico</th>
                                    <th align="center">Edición</th>

                                </tr>
                                <%while (itIngresopersona.hasNext()) {
        Ingresopersona persona = itIngresopersona.next();%>
                                <tr bgcolor="#FFF">

                                    <td><%= persona.getNombre()%></td>

                                    <td><%= persona.getApellido()%></td>

                                    <td><%= persona.getEdad()%></td>
                                    <td><%= persona.getCarrera()%></td>

                                    <td><%= persona.getCorreo()%></td>

                                    <td><a href="editarController?nombre=<%= persona.getNombre()%>">Editar</a> / <a href="eliminarController?nombre=<%= persona.getNombre()%>">Eliminar</a></td>
                                    <%}%>
                                </tr>
                                </form>  
                            </table>
                        </fieldset>
                    </td>    
                </tr>
            </table>
    </body>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
</html>
