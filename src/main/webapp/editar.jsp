<%-- 
    Document   : tabla
    Created on : 5/06/2021, 05:09:41 PM
    Author     : Josefina
--%>

<%@page import="cl.entity.Ingresopersona"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%  Ingresopersona datosPersona = (Ingresopersona) request.getAttribute("datosPersona");
    if (datosPersona == null) {
        response.sendRedirect("tablaController");
    }%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="styles/style.css" rel="stylesheet" type="text/css">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    </head>
    <body>
        <div class="container mt-2"> 
            <div class="card" style="width: 18rem;" border="1">

                <form  name="form" action="editarController" method="POST">

                    <h1>Edición de Datos</h1>

                    <label for="nombre">Nombre: </label>

                    <input type="text" name="nombre" id="nombre" readonly="readonly" value="<%= datosPersona.getNombre()%>">

                    <br/> <br/>


                    <label for="apellido">Apellido: </label>

                    <input type="text" name="apellido" id="apellido" value="<%= datosPersona.getApellido()%>">

                    <br/> <br/>

                    <label for="edad">Edad: </label>

                    <input type="text" name="edad" id="edad" value="<%= datosPersona.getEdad()%>">

                    <br/> <br/>

                    <label for="carrera">Carrera: </label>

                    <input type="text" name="carrera" id="carrera" value="<%= datosPersona.getCarrera()%>">

                    <br/> <br/>

                    <label for="correo">Mail: </label>

                    <input type="text" name="correo" id="correo" value="<%= datosPersona.getCorreo()%>">

                    <br/> <br/>
                    <button type="submit" value="ingresar" class="btn btn-success">Editar</button>
                    <button type="button" value="ingresar" class="btn btn-success" onclick="window.location.href = 'tablaController';">Cancelar</button>
                </form>


            </div>  
            <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
    </body>
</html>
