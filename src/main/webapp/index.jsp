<%-- 
    Document   : index
    Created on : 5/06/2021, 04:26:49 PM
    Author     : Josefina
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="styles/style.css" rel="stylesheet" type="text/css">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
        <title>Ingreso de Datos</title>
    </head>

    <body>


        <div class="container mt-2"> 
            <div class="card" style="width: 18rem;" border="1">

                <form  name="form" action="ingresoController" method="POST">

                    <h1>Ingreso de Datos</h1>

                    <label for="nombre">Nombre: </label>

                    <input type="text" name="nombre" id="nombre">

                    <br/> <br/>


                    <label for="apellido">Apellido: </label>

                    <input type="text" name="apellido" id="apellido">

                    <br/> <br/>

                    <label for="edad">Edad: </label>

                    <input type="text" name="edad" id="edad">

                    <br/> <br/>

                    <label for="carrera">Carrera: </label>

                    <input type="text" name="carrera" id="carrera">

                    <br/> <br/>

                    <label for="correo">Mail: </label>

                    <input type="text" name="correo" id="correo">

                    <br/> <br/>
 <button type="submit" value="ingresar" class="btn btn-success">Enviar</button>
                </form>

                
</div>  
                </body>

            
            <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
</html>
